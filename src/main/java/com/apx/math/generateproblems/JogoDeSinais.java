
package com.apx.math.generateproblems;

/**
 *
 * @author joaocassamano
 */

import java.util.Random;
import java.util.Scanner;

public class JogoDeSinais {
    static int NUM_EXERCISES=5;
    public static void main(String[] args){
        
        System.out.println("Select the number of the operation:");
        System.out.println("1. Jogo de sinais");
        System.out.println("2. Decompoe em factores primos");
        System.out.println("3. Simplifique as fracoes");
        System.out.println("4. Simplifique as fracoes(sem ou com poucos numeros primos)");
        //System.out.println("5. Inequacoes");
        
        
        Scanner scan = null;
        try{
            scan = new Scanner(System.in);
            int option = scan.nextInt();
            
            if(option ==1){
                for(int i=0;i<NUM_EXERCISES;i++){ signs();}
            } else if(option ==2){
                for(int i=0;i<NUM_EXERCISES;i++){ decomposeIntoPrimes(i);}
            } else if(option ==3){
                for(int i=0;i<NUM_EXERCISES;i++){ simplifyFractions(i,false);}
            } else if(option ==4){
                for(int i=0;i<NUM_EXERCISES;i++){ simplifyFractionComposites(i);}
            }  else if(option ==5){
                //for(int i=0;i<NUM_EXERCISES;i++){ inequacoes(i);}
            }else{
                
            }
        }catch(Exception e){
        
        }finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occurred closing scanner due to "+e.getMessage());}
        }
        
    }
    
    public static void signs(){
        Random rn = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Por favor entre um numero de termos:");
        int terms = 0;

        try{
            terms = scan.nextInt();
            if(terms>=2 && terms<=10)
                lines(rn,terms);
            else
                System.out.println("Por favor entre um numero inteiro valido ie. entre 2 e 10");
        }catch(Exception e){
            System.out.println("Por favor entre um numero inteiro valido ie. entre 2 e 10");
        }
    }
    
    public static void decomposeIntoPrimes(int i){
        Random rn = new Random();
        int value = (rn.nextInt(100));
        
        value = (value==0)? (2+rn.nextInt(100)) : value;//cannot be zero
        System.out.print((i+1)+". ");
        System.out.println(value);
    }
    
    public static void simplifyFractions(int i, boolean composites){
        Random rn = new Random();
        String fraction="";
        int num = generateNumber(rn,composites,0);
        int den = generateNumber(rn,composites,0);
        
        if ((num > 0 && den > 0) || (num < 0 && den < 0) ){
            if(num < 0){
                num = (-1)*num;
                den = (-1)*den;
            }
            fraction = num+"/"+den;
        } else{
            if(num < 0){num = (-1)*num;}
            if(den < 0){den = (-1)*den;}
            fraction = "-"+num+"/"+den;
        }
        System.out.print((i+1)+". ");
        System.out.println(fraction);
    }
    
    public static int generateNumber(Random rn, boolean composites, int max){
        int localMax = 100;
        max = max < 0? (-1)*max: max;
        localMax = max==0? localMax:max;
        int sign = (rn.nextInt(12))%2;
        int num = (rn.nextInt(localMax));
        if(composites){num = primeToComposite(num,rn);}
        num = (num==0)? (2+rn.nextInt(localMax)) : num;//cannot be zero
        num = sign==0? num: (-1)*num;
        
        return num;
    }
    
    protected static int primeToComposite(int num, Random rn){
        SistemaDeEquacoesLineares sde = new SistemaDeEquacoesLineares();
        int count =0;
        while(sde.isPrime(num) || num < 2){
            num = (rn.nextInt(100));
            num = (num==0)? (2+rn.nextInt(100)) : num;//cannot be zero
            count++;
            if(count>99) return num;
        }
        return num;
    }
    
    //simplifyFractionComposites(i)
    public static void simplifyFractionComposites(int i){
        simplifyFractions(i, true);
    }
    
    public static void lines(Random rn, int terms){
        int examples=25;
        System.out.println("Imprimindo "+examples+" exemplos:");
        for(int i=0;i<examples;i++){
            int value =0;

            String line ="";
            int counter=0;

            //sign
            value = (rn.nextInt(12))%2;
            line+=value==1?"-":"";

            //number
            value = 1+rn.nextInt(12);
            line+=value;

            counter++;

            while(counter<terms){
                //sign
                value = (rn.nextInt(12))%2;
                line+=value==1?"+":"-";

                //number
                value = 1+rn.nextInt(12);
                line+=value;
                counter++;
            }
            System.out.println(line);
        }
    }
}
