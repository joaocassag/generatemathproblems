
package com.apx.math.generateproblems;

/**
 *
 * @author joaocassamano
 */

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.math.*;
import java.math.BigDecimal;

public class Polinomios {
    int NUMBER_OF_EXERCISES=5;
    int OP_DIV_LONG=1;
    int OP_DIV_RUFF=2;
    int OP_REST_THRM=3;
    int OP_FACTORIZ=4;
    int OP_EQ_QUADR=5;
    int OP_ALG_FRACC=6;
    int OP_ALG_FRACC_ADD=7;
    int OP_ALG_FRACC_MUL=8;
    int OP_EQ_BIQUAD=9; 
    
    boolean debug=false;
    boolean modeX= false;
    boolean actionX = false;
    
    public static void main(String[] args){
        Polinomios ea = new Polinomios();
        ea.processCommandLineArguments(args);
        ea.process(ea);
    }
    
    public void processCommandLineArguments(String args[]){
        for(int i=0; i< args.length; i++){
            if(args[i].equals("-debug")){
                debug = true;
            } else if(args[i].equals("-modeX")){
                modeX = true;//TODO: customize
            } else if(args[i].equals("-actionX") && i+1<args.length){
                actionX=true;//TODO: customize
            }
        }
    }
    
    public void process(Polinomios ea){
        if(debug){System.out.println("Entered divisaoInteiraDePolinomios");}
        String type = "";
        Scanner scan = null;
        Random randNum = new Random();
        
        try{
            scan = new Scanner(System.in);
            System.out.println("#1 Digite I para gerar termos com numeros inteiros ou F com fraccoes e pressione Enter ");
            type = scan.next();
        }catch(Exception e){
            System.out.println("#2 Digite I para gerar termos com numeros inteiros ou F com fraccoes e pressione Enter ");
            if(debug){System.out.println("Leaving divisaoInteiraDePolinomios");}
            return;
        }
        
        if(!type.equalsIgnoreCase("i") && !type.equalsIgnoreCase("f")){
            System.out.println("#3 Digite I para gerar termos com numeros inteiros ou F com fraccoes e pressione Enter ");
            if(debug){System.out.println("#4 Leaving divisaoInteiraDePolinomios");}
            return;
        }
        
        System.out.println("Gerando "+NUMBER_OF_EXERCISES+" exercicios ...");
        System.out.println("Entre a informacao requerida abaixo:");
        
        int operationType = getOperationType(scan, type);
        
        for(int i=0;i<NUMBER_OF_EXERCISES;i++){
            
            //type = "i";
            int pDegree = ea.nTerms(); //pDegree();
            //pDegree=1;
            //System.out.println("pDegree is "+pDegree);
            LinkedList<String> pCoeff = null;
            
            if(operationType!=OP_EQ_QUADR && operationType!=OP_EQ_BIQUAD && operationType!=OP_ALG_FRACC && operationType!=OP_ALG_FRACC_ADD)pCoeff = ea.generateTerms(pDegree+1, type);
            
            if(operationType==OP_EQ_QUADR || operationType==OP_EQ_BIQUAD || operationType==OP_ALG_FRACC || operationType == OP_ALG_FRACC_ADD){
                outputOP_(operationType,pDegree,pCoeff,ea,type,i,randNum);
            }
            else if(operationType==OP_FACTORIZ){
                if(debug){System.out.println("#6 operationType==OP_FACTORIZ: "+operationType);}
                
                LinkedList<String> mCoeff =  generateTerms(2,type);
                if(debug){
                    String[] polm = ea.polyn(mCoeff,2);
                    System.out.print("Init d(x)=");
                    ea.printPol(polm);
                }
                
                LinkedList<String> extPol=multiply(pCoeff,pDegree,mCoeff,type);
                
                String[] finalPol = ea.polyn(extPol,pDegree+1+1);
                
                System.out.print("P(x)=");
                ea.printPol(finalPol);
                
                //double a = solveMonomial(mCoeff,"");
                String a = solveMonomial(mCoeff,"");
                System.out.println("Factorize P(x) sabendo que "+a+ " e' um zero do polinómio");
                
                //BigDecimal rest = ea.findRest(extPol,pDegree+1+1,a);
                //System.out.println("P("+a+") is "+rest.doubleValue());
                
                //System.out.println("Final pol is: ");
                //printStringLL(finalPol);
            }
            else if(operationType==OP_DIV_LONG || operationType ==OP_REST_THRM || operationType==OP_DIV_RUFF){
                if(debug){System.out.println("#7 operationType==OP_DIV_LONG || operationType ==OP_REST_THRM || operationType==OP_DIV_RUFF "+operationType);}
                if(operationType==OP_DIV_LONG || operationType ==OP_REST_THRM) System.out.println("O grau de P(x) é "+pDegree);
            
                String[] d = operation(operationType,pDegree,scan,ea,type);

                System.out.println("Exercício "+(i+1)+":");
                System.out.print("P(x)=");
                String[] pol = ea.polyn(pCoeff,pDegree+1);
                ea.printPol(pol);
                //System.out.println();

                System.out.print("D(x)=");
                ea.printPol(d);
                System.out.println();
            }
        }
        if(debug){System.out.println("#8 Leaving divisaoInteiraDePolinomios");}
    }
    
    public String[] polyn(LinkedList<String> coeff, int degree){
        if(debug){System.out.println("1 Entered polyn");}
        String [] pol = new String[degree];
        int j=0;
        if(debug){
            System.out.println("coeff is: ");
            for(String s: coeff){System.out.print(s);}
            System.out.println();
        }
        for(int i=2;i<coeff.size();i++){
            if( !coeff.get(i).equals("-") && !coeff.get(i).equals("+") ){
                continue;
            }
            if(debug){
                System.out.println("i is "+i);
                System.out.println("coeff.get(i) is "+coeff.get(i));
                System.out.println("coeff.get(i+2) is "+coeff.get(i+2));
            }
            if((degree-1-j) == 0) pol[j] = coeff.get(i)+""+coeff.get(i+2);
            else if((degree-1-j) == 1) pol[j] = coeff.get(i)+""+coeff.get(i+2)+"x^1";
            else pol[j] = coeff.get(i)+""+coeff.get(i+2)+" x^"+(degree-1-j);
            j++;
            i+=2;// skip next space and next number
            //if(j==(degree+1))break;
        }
        if(debug){System.out.println("2 Leaving polyn");}
        return pol;
    }
    
    public void printPol(String[] pol){
        if(debug){System.out.println("Entered printPol");}
        for(int i=0;i<pol.length;i++){
            System.out.print(pol[i]+" ");
        }
        System.out.println();
        if(debug){System.out.println("Leaving printPol");}
    }
    
    public String stringifyPol(String[] pol){
        if(debug){System.out.println("Entered stringifyPol");}
        String fin="";
        for(int i=0;i<pol.length;i++){
            fin+=pol[i]+" ";
        }
        if(debug){System.out.println("Leaving stringifyPol");}
        return fin;
    }
    
    public BigDecimal findRest(LinkedList<String> coeff, int degree, double a){
        if(debug){System.out.println("Entered findRest");}
        int j=0;
        if(debug){
            System.out.println("coeff is: ");
            for(String s: coeff){System.out.print(s);}
            System.out.println();
        }
        BigDecimal zero = new BigDecimal("0");
        System.out.println("degree is "+degree);
        
        //double zero = 0;
        for(int i=2;i<coeff.size();i++){
            if( !coeff.get(i).equals("-") && !coeff.get(i).equals("+") ){
                continue;
            }
            if(debug){
                System.out.println("j is "+j);
                System.out.println("i is "+i);
                System.out.println("coeef.get(i) is "+coeff.get(i));
                System.out.println("coeef.get(i+2) is "+coeff.get(i+2));
            }
            int coeffValue = Integer.parseInt(coeff.get(i+2));
            coeffValue = coeff.get(i).equals("-")? -1*coeffValue:coeffValue;
            if((degree-1-j) == 0) {
                System.out.println("coeffValue is "+coeffValue);
                zero = zero.add(new BigDecimal(""+coeffValue));
            }
            else if((degree-1-j) == 1) {
                BigDecimal d1 = new BigDecimal(""+coeffValue);
                BigDecimal d1_times_a = d1.multiply(new BigDecimal(""+a));
                System.out.println("coeffValue is "+coeffValue+" a is "+a+ " coeffValue * a is "+d1_times_a);
                zero = zero.add(d1_times_a);
            }
            else {
                BigDecimal d1 = new BigDecimal(""+coeffValue);
                BigDecimal d1_times_a = d1.multiply(new BigDecimal(""+ (Math.pow(a, degree-1-j) )) );
                System.out.println("coeffValue is "+coeffValue+" a is "+a+ " coeffValue * a^"+(degree-1-j)+" is "+d1_times_a);
                zero = zero.add(d1_times_a);
            }
            j++;
            i+=2;// skip next space and next number
            //if(j==(degree+1))break;
            System.out.println("zero is "+zero.doubleValue());
        }
        if(debug){System.out.println("Leaving findRest");}
        return zero;
    }
    
    public LinkedList<String> generateTerms(int nTerms, final String type){
        if(debug){System.out.println("Entered generateTerms");}
        //Random rn = new Random();
        LinkedList<String> l = new LinkedList<>();
        l.add(" ");
        int sign =0;
        
        for(int i=0;i<nTerms;i++){
            Random rn = new Random();
            sign = (rn.nextInt(2))%2;
            l.add(" ");
            l.add(sign==0?"+":"-");
            l.add(" ");
            
            String term = getTerms(type);
            l.add(term);
            l.add(" ");
        }
        if(debug){System.out.println("Leaving generateTerms");}
        return l;
    }
    
    public String getTerms(final String type){
        if(debug){System.out.println("Entered getTerms");}
        String _type = type;
        if(!type.equalsIgnoreCase("i") && !type.equalsIgnoreCase("f")){
            System.out.println("Warning: Did not provide F or I, setting I by default ...");
            _type="i";
        }
        
        String value="";
        int[] numbers = fractionNumbers();
        if(_type.equalsIgnoreCase("i")){
           value=numbers[0]+"";
        } else if(_type.equalsIgnoreCase("f")){
            value=numbers[0]+"/"+numbers[1];
        }
        if(debug){System.out.println("Leaving getTerms");}
        return value;
    }
    
    public int[] fractionNumbers(){
        if(debug){System.out.println("Entered fractionNumbers");}
        Random rn = new Random();
        //Random rn = null;
        int[] numbers = new int[2];
        int n=2;
        for(int i=0;i<2;i++){
            //rn = new Random(System.nanoTime());
            n= (1+rn.nextInt(4));
            if (n==1){n= (i==1||i==3)? 2 : n;}
            
            numbers[i] = n;
        }
        if(debug){System.out.println("Leaving fractionNumbers");}
        return numbers;
    }
    
    public int nTerms(){
        if(debug){System.out.println("Entered nTerms");}
        Random rn = null;
        /*if(type.equalsIgnoreCase("i")){
            rn = new Random(System.nanoTime());
            return (2+rn.nextInt(4));
        }*/
        rn = new Random(System.nanoTime());
        if(debug){System.out.println("Entered nTerms");}
        return (2+rn.nextInt(4));
    }
    
    public int nTerms(int max){
        if(debug){System.out.println("Entered nTerms(..)");}
        Random rn = null;
        rn = new Random(System.nanoTime());
        if(debug){System.out.println("Leaving nTerms(..)");}
        return (1+rn.nextInt(max));
    }

    private String[] operation(int operationType, int pDegree, Scanner scan, Polinomios ea, String type) {
        if(debug){System.out.println("Entered operation");}
        int dDegree ;//= ea.nTerms(pDegree-1);
        if(operationType ==OP_DIV_RUFF || operationType ==OP_REST_THRM){
            if(operationType == OP_DIV_RUFF){
                
            } else if (operationType ==OP_REST_THRM){
                System.out.println("Ache o resto e indica se \"a\" é um zero de P(x)");
            }
            dDegree = 1;
            LinkedList<String> dCoeff = ea.generateTerms(dDegree+1, type);
            String[] d = ea.polyn(dCoeff,dDegree+1);
            if(debug){System.out.println("Leaving operation");}
            return d;
        }
        
        System.out.println("Escolha o grau de D(x) ie. um numero entre 1 e "+(pDegree-1));
        
        try{
            //scan = new Scanner(System.in);
            dDegree = scan.nextInt();
            if(dDegree <0 || dDegree>=pDegree){
                dDegree = ea.nTerms(pDegree-1);
                System.out.println("Warning: Occorreu um erro. O grau de D(x) escolhido foi "+dDegree);
            }
        }catch(Exception e){
            dDegree = ea.nTerms(pDegree-1);
            System.out.println("Warning: Occorreu um erro. O grau de D(x) escolhido foi "+dDegree);
        }

        LinkedList<String> dCoeff = ea.generateTerms(dDegree+1, type);
        String[] d = ea.polyn(dCoeff,dDegree+1);
        if(debug){System.out.println("Leaving operation");}
        return d;
    }

    private int getOperationType(Scanner scan, String type) {
        if(debug){System.out.println("Entered getOperationType");}
        int divType=0;
        System.out.println("Entre um opcao: \n"
                + "(1) Divisao inteira \n"
                + "(2) Ruffini \n"
                + "(3) Teorema do resto \n"
                + "(4) Factorizacao \n"
                + "(5) Equacao quadratica \n"
                + "(6) Express. Algreb. Fraccionaria \n"
                + "(7) Addicao de Express. Algreb. Fraccionaria \n"
                + "(8) Multiplicacao de Express. Algreb. Fraccionaria \n");
        
        try{
            divType = scan.nextInt();
            if(divType == OP_ALG_FRACC_MUL) divType = OP_ALG_FRACC_ADD;
            
            if(divType <OP_DIV_LONG || divType>OP_ALG_FRACC_MUL){
                divType=OP_DIV_LONG;
                System.out.println("Warning: Occorreu um erro. Divisao inteira foi selecionada. ");
            }
        }catch(Exception e){
            divType=OP_DIV_LONG;
            System.out.println("Warning: Occorreu um erro. Divisao inteira foi selecionada. ");
        }
        
        if(divType==OP_FACTORIZ || divType==OP_EQ_QUADR|| divType==OP_ALG_FRACC || divType == OP_ALG_FRACC_ADD){
            if("i".equalsIgnoreCase(type)){
                type="i";
                System.out.println("Warning: Este programa nao e' permite o uso de fracoes quando utilizando a funcionalidade de factorizacao.  ");
                System.out.println("NOTA: usando numreos inteiros!");
            }
        }
        if(debug){System.out.println("Leaving getOperationType");}
        return divType;
    }

    protected LinkedList<String> multiply(LinkedList<String> pCoeff,int pDegree, LinkedList<String> mCoeff,final String type) {
        if(debug){System.out.println("Entered multiply");}
        LinkedList<String> extPol =new LinkedList<>();
        extPol.add(" ");
        LinkedList<Integer> extDeg =new LinkedList<>();
        //LinkedList<String> mCoeff =  generateTerms(2,type);
        //String[] polyn(mCoeff,1)
        int md=2;
        for(int i=1;i<mCoeff.size();i++){ 
            if( !mCoeff.get(i).equals("-") && !mCoeff.get(i).equals("+") ){continue;}
            
            String m_sign = (mCoeff.get(i));
            int m = Integer.parseInt(mCoeff.get(i+2));
            md--;
            int cd=pDegree+1;
            for(int j=1;j<pCoeff.size();j++){
                if( !pCoeff.get(j).equals("-") && !pCoeff.get(j).equals("+") ){continue;}
                
                String c_sign = (pCoeff.get(j));
                int c = Integer.parseInt(pCoeff.get(j+2));
                cd--;
                //sign
                String sign = m_sign.equals(c_sign)?"+":"-";
                extPol.add(" ");
                extPol.add(sign);
                extPol.add(" ");
                //coeff
                extPol.add(""+m*c);
                //extPol.add(" ");
                //degree
                extDeg.add(md+cd);
            }
        }
        LinkedList[] vars = new LinkedList[3];
        vars[0]=(mCoeff);
        vars[1]=(pCoeff);
        vars[2]=(extPol);
        //printMultLL(extDeg,mCoeff,pCoeff,extPol);
        
        int midP = 2+4*extDeg.get(0);//System.out.println("id is: "+( 2+4*extDeg.get(0)) );
        LinkedList<String> finalPol =add(extPol,extDeg,midP);
        if(debug){System.out.println("Leaving multiply");}
        return finalPol;
    }
    
    private LinkedList<String> add(LinkedList<String> extPol,LinkedList<Integer> extDeg, int midP){
        if(debug){System.out.println("Entered add");}
        LinkedList<String> finalPol = new LinkedList<>(); 
        finalPol.add(" ");
        
        finalPol.add(" ");
        //highest degree
        finalPol.add(extPol.get(2));//sign
        finalPol.add(extPol.get(3));//space
        finalPol.add(extPol.get(4));//number
        
        int j=midP;
        for(int i=6;i<midP;i++){
            String c_sign = (extPol.get(i));
            int c ="-".equals(c_sign)? (-1)*Integer.parseInt(extPol.get(i+2)):Integer.parseInt(extPol.get(i+2));
            //int c =Integer.parseInt(extPol.get(i+2));
            //System.out.println("i "+i+" "+c_sign+" "+c);
            
            String m_sign = (extPol.get(j));
            int m ="-".equals(m_sign)? (-1)*Integer.parseInt(extPol.get(j+2)):Integer.parseInt(extPol.get(j+2));
            //int m =Integer.parseInt(extPol.get(j+2));
            //System.out.println("j "+j+" "+m_sign+" "+m);
            
            i+=3;//increment by 3
            j+=4;//b/c not a for loop index
            System.out.println();
            
            int val = m+c;
            //sign
            String sign = val>=0?"+":"-";
            finalPol.add(" ");
            finalPol.add(sign);
            finalPol.add(" ");
            //coeff
            finalPol.add(""+( val<0? (-1)*val:val ) );
        }
        //j-=1;
        //System.out.println("j "+j+" "+extPol.get(j)+" "+extPol.get(j+2));
        //independent term
        finalPol.add(" ");
        finalPol.add(extPol.get(j));
        finalPol.add(" ");
        //coeff
        finalPol.add(extPol.get(j+2));
        
        if(debug){
            System.out.println("Final pol is: ");
            printStringLL(finalPol);
        }
        if(debug){System.out.println("Leaving add");}
        return finalPol; 
    }
    
    private static void printStringLL(LinkedList<String> l){
        for(String s: l){System.out.print(s);}
        System.out.println();
    }
    
    private static void printIntegerLL(LinkedList<Integer> l){
        for(int s: l){System.out.print(s+" ");}
        System.out.println();
    }
    
    private static void printMultLL(LinkedList<Integer> extDeg,LinkedList<String> mCoeff, LinkedList<String> pCoeff, LinkedList<String> extPol ){
        System.out.println("d(x): ");
        printStringLL(mCoeff);
        
        System.out.println("P(x): ");
        printStringLL(pCoeff);
        
        System.out.println("extPol: ");
        printStringLL(extPol);
        
        System.out.println("extDeg: ");
        printIntegerLL(extDeg);
        
    }

    private String solveMonomial(LinkedList<String> mCoeff) {
        if(debug){
            System.out.println("Entered solveMonomial");
            System.out.println("mCoeff: ");
            printStringLL(mCoeff);
        }
        
        String a = "";
        String num = "";
        String den = "";
        String num_sign = "";
        String den_sign = "";
        boolean samelength=false;
        
        for(int i=0;i<mCoeff.size();i++){
            if( !mCoeff.get(i).equals("-") && !mCoeff.get(i).equals("+") ){
                continue;
            }
            den_sign=mCoeff.get(i);
            den = mCoeff.get(i+2);
            
            den = "1".equals(mCoeff.get(i+2))?"":("/"+mCoeff.get(i+2));
            
            i+=5;
            num_sign=mCoeff.get(i);
            num = mCoeff.get(i+2);
            if(num.equals(den)){
                samelength=true;
            }
            
            break;
        }
        
        //System.out.println("num_sign: "+num_sign);
        //System.out.println("den_sign: "+den_sign);
        //System.out.println("num: "+num);
        //System.out.println("den: "+den);
        if(num_sign.charAt(0) == den_sign.charAt(0)){
            if(samelength)a="1";
            else a = num+den;
        }
        else{
            if(samelength)a="-1";
            else a = "-"+num+den;
        }
        //System.out.println("a is "+a);
        //System.out.println("Leaving solveMonomial");
        if(debug){System.out.println("Leaving solveMonomial");}
        return a;
    }
    
    protected String solveMonomial(LinkedList<String> mCoeff,String zero) {
        if(debug){
            System.out.println("Entered solveMonomial(..,)");
            System.out.println("mCoeff: ");
            printStringLL(mCoeff);
        }
        
        String a = "";
        String num = "";
        String den = "";
        String num_sign = "";
        String den_sign = "";
        boolean samelength=false;
        
        for(int i=0;i<mCoeff.size();i++){
            if( !mCoeff.get(i).equals("-") && !mCoeff.get(i).equals("+") ){
                continue;
            }
            den_sign=mCoeff.get(i);
            den = mCoeff.get(i+2);
            i+=5;
            num_sign=mCoeff.get(i);
            num = mCoeff.get(i+2);
            
            //System.out.println("(num_sign+num): "+num_sign+" "+num);
            //System.out.println("(den_sign+den): "+den_sign+" "+den);
            if(debug){System.out.println("Leaving solveMonomial(..,)");}
            int snum = num_sign.equals("-")? Integer.parseInt(num):(-1)*Integer.parseInt(num);
            int sden = den_sign.equals("-")? (-1)*Integer.parseInt(den):Integer.parseInt(den);
            String ret = SistemaDeEquacoesLineares.simplifyFraction(snum,sden);
            return ret;
            //return -Double.parseDouble(num_sign+num)/Double.parseDouble(den_sign+den);
        }
        
        //System.out.println("num_sign: "+num_sign);
        //System.out.println("den_sign: "+den_sign);
        //System.out.println("num: "+num);
        //System.out.println("den: "+den);
        if(num_sign.charAt(0) == den_sign.charAt(0)){
            if(samelength)a="1";
            else a = num+den;
        }
        else{
            if(samelength)a="-1";
            else a = "-"+num+den;
        }
        //System.out.println("a is "+a);
        if(debug){System.out.println("Leaving solveMonomial(..,)");}
        return "0";
    }

    protected void outputOP_(int operationType, int pDegree, LinkedList<String> pCoeff, Polinomios ea, String type, int i, Random randNum) {
        if(debug){System.out.println("#5 operationType==OP_EQ_QUADR || operationType==OP_EQ_BIQUAD || "
                        + "operationType==OP_ALG_FRACC || "
                        + "operationType == OP_ALG_FRACC_ADD: "+operationType);}
        pDegree=1;
        pCoeff = ea.generateTerms(pDegree+1, type);
        if(debug){
            System.out.print("Init P(x)=");
            String[] pol = ea.polyn(pCoeff,pDegree+1);
            ea.printPol(pol);
        }

        LinkedList<String> mCoeff =  generateTerms(2,type);
        if(debug){
            System.out.print("Init d(x)=");
            String[] polm = ea.polyn(mCoeff,2);
            ea.printPol(polm);
        }

        LinkedList<String> extPol=multiply(pCoeff,pDegree,mCoeff,type);

        String[] finalPol = ea.polyn(extPol,pDegree+1+1);

        if(operationType==OP_EQ_QUADR || operationType==OP_EQ_BIQUAD){
            if(operationType==OP_EQ_BIQUAD){
                finalPol[0]=finalPol[0].replaceAll("\\^2", "\\^4");
                finalPol[1]=finalPol[1].replaceAll("\\^1", "\\^2");
            }
            System.out.print("P(x)=");
            ea.printPol(finalPol);
            //double a = solveMonomial(mCoeff,"");
            System.out.println("Find the roots of P(x)");
        }

        if(operationType==OP_ALG_FRACC || operationType==OP_ALG_FRACC_ADD){
            if(i==0)System.out.println("Simplifique a expressao algebrica e determina o dominio");
            int problemNum=i+1;
            System.out.println(problemNum+".");
            String[] polm = ea.polyn(mCoeff,2);

            if(operationType==OP_ALG_FRACC)
                System.out.println("a)");
            if(operationType==OP_ALG_FRACC_ADD)
                System.out.println("Term 1");
            System.out.print("Numerador:");
            int numX = 1 + randNum.nextInt(5);
            System.out.println(numX);
            System.out.print("Denominador: ");
            ea.printPol(polm);

            if(operationType==OP_ALG_FRACC)
                System.out.println("b)");
            if(operationType==OP_ALG_FRACC_ADD)
                System.out.println("Term 2");

            System.out.print("Numerador:");
            randNum = new Random();
            numX = 1 + randNum.nextInt(5);
            System.out.println(numX);
            System.out.print("Denominador: ");
            ea.printPol(finalPol);

            if(operationType==OP_ALG_FRACC){
                System.out.println("c)");
                System.out.print("Numerador:");
                ea.printPol(polm);
                System.out.print("Denominador: ");
                ea.printPol(finalPol);
            }
        }
    }
}
