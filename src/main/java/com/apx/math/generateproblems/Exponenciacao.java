
package com.apx.math.generateproblems;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class Exponenciacao {
    
    static int NUM_EXERCISES=5;
    
    public static void main(String[] args){
        System.out.println("Select the number of the operation:");
        System.out.println("1. Encontrar o dominio (e expressa os intervalos)");
        System.out.println("2. Racionalizacao do denominador");
        System.out.println("3. Equacoes biquadraticas");
        System.out.println("4. Equacoes com radicais");
        System.out.println("5. Inequacoes");
        System.out.println("6. Tabela de sinais e intervalos");
        System.out.println("7. Equacoes exponenciais");
        System.out.println("8. Equacoes exponenciais quadraticas");
        System.out.println("9. Inequacoes exponenciais");
        System.out.println("10. Inequacoes exponenciais quadraticas");
        
        
        Scanner scan = null;
        Exponenciacao exp = new Exponenciacao();
        try{
            scan = new Scanner(System.in);
            int option = scan.nextInt();
            //inequacoesSignTablePlusDomainOnly(int n)
            if(option ==1){
                exp.findDomainOfFunction();
            } else if(option ==2){
                for(int i=0;i<NUM_EXERCISES;i++){ exp.racionalizationOfDenominator();}
            } else if(option ==3){
                for(int i=0;i<NUM_EXERCISES;i++){ exp.equacoesBiquadraticas();}
            } else if(option ==4){
                for(int i=0;i<NUM_EXERCISES;i++){ exp.equacoesComRadicais();}
            } else if(option ==5){
                for(int i=0;i<NUM_EXERCISES;i++){ exp.inequacoes(i);}
            } else if(option ==6){
                for(int i=0;i<NUM_EXERCISES;i++){ exp.inequacoesSignTablePlusDomainOnly(i);}
            } else if(option ==7){
                System.out.println("Resolva as equacoes exponenciais ");
                for(int i=0;i<NUM_EXERCISES;i++){ exp.equacoesExponenciais(i,1,false);}
            } else if(option ==8){
                System.out.println("Resolva as equacoes exponenciais ");
                for(int i=0;i<NUM_EXERCISES;i++){ exp.equacoesExponenciaisQuadratica(i,2,false);}
            } else if(option ==9){
                System.out.println("Resolva as inequacoes exponenciais ");
                for(int i=0;i<NUM_EXERCISES;i++){ exp.inequacoesExponenciais(i,1,true);}
            } else if(option ==10){
                System.out.println("Resolva as inequacoes exponenciais ");
                for(int i=0;i<NUM_EXERCISES;i++){ exp.inequacoesExponenciaisQuadratica(i,2,true);}
            }else{
                
            }
        }catch(Exception e){
        
        }finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occurred closing scanner due to "+e.getMessage());}
        }
    }
    
    public void findDomainOfFunction(){
        
        Polinomios p = new Polinomios();
        Scanner scan = null;
        try{
            scan = new Scanner(System.in);
            //int pDegree = p.nTerms(2);
            System.out.println("Please choose the degree of the polinomial expression: 1,2, or 3");
            int pDegree = scan.nextInt();
            if(pDegree<1 || pDegree>3){
                pDegree = p.nTerms(2);
                System.out.println("Allowed degrees have to be between 1,2, or 3. Resetting .... Reset to "+pDegree);
            }
            Random r = new Random();
            System.out.println("Please choose the index of root: 2 or 3");
            int root = scan.nextInt();
            if(root<1 || pDegree>3){
                //Random r = new Random();
                root = ((r.nextInt(4))%2) == 0? 2 : 3;
                System.out.println("Allowed indices 2 and 3. Resetting .... Reset to "+root);
            }
            String type="i";
            LinkedList<String> pCoeff = p.generateTerms(pDegree+1, type);

            String[] pol = p.polyn(pCoeff,pDegree+1);
            String sPol = p.stringifyPol(pol);
            System.out.println("Fraccionaria(F) ou nao fraccionaria(N)?");
            String fraccao = scan.next();
            int num=-1;
            if(fraccao.equalsIgnoreCase("f")){
                num = (1+r.nextInt(5));
                num = (1+r.nextInt(4))%2==0? num:(-1)*num;
                System.out.println( num+"/"+caretToExponent(singleDigitRoot(""+root,sPol)) );
            } else{
                System.out.println( caretToExponent(singleDigitRoot(""+root,sPol)) );
            }
            System.out.println();
        }
        catch(Exception e){}
        finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occurred closing scanner due to "+e.getMessage());}
        }
    }
    
    public void racionalizationOfDenominator(){
        Random r = new Random();
        int[] values = rootExpNumAlg(r);
        int root = values[0];
        int exp = values[1];
        int num = values[2];
        int alg = values[3];
        String radicand = "";
        String den="";
        num = (1+r.nextInt(4))%2==0? num:(-1)*num;
        if(alg==0){
            radicand+= (2+r.nextInt(5))+"^"+exp;
            den = caretToExponent(singleDigitRoot(""+root,radicand));
        }
        else if(alg==1){
            radicand= (2+r.nextInt(5))+"^"+exp;
            //radicand = (1+r.nextInt(4))%2==0? "+"+radicand:"-"+radicand;
            den = caretToExponent(singleDigitRoot(""+2,radicand));
            
            values = rootExpNumAlg(r);
            values[1]= (exp%2 != 0)? values[1] : ( values[1]%2 != 0? values[1]: ++values[1]);
            String term2=(2+r.nextInt(5))+"^"+values[1];
            term2=caretToExponent(singleDigitRoot(""+2,term2));
            //term2 = (1+r.nextInt(4))%2==0? "+"+term2:"-"+term2;
            
            String sign = (1+r.nextInt(4))%2==0? "+":"-";
            den= "("+den+sign+term2+")";
        }
        else {
            radicand= (1+r.nextInt(5))+"x^"+exp;
            den = caretToExponent(singleDigitRoot(""+root,radicand));
        }
        
        System.out.println( num+"/"+den);
    }
    
    public void equacoesBiquadraticas(){
        LinkedList<String> pCoeff = null;
        Polinomios ea = new Polinomios();
        Random randNum = new Random();
        String type = "i";
        int i =0;
        int pDegree = 2;
        int operationType = 9;//9 == biquadratic equation
        ea.outputOP_(operationType,pDegree,pCoeff,ea,type,i,randNum);
    }
    
    public void equacoesComRadicais(){
        Random r = new Random();
        int[] values = rootExpNumAlg(r);
        int root = values[0];
        int exp = (1+r.nextInt(4))%2==0? 2 : 1;
        int num = values[2];
        int alg = values[3];
        String radicand = "";
        String fin="";
        num = (1+r.nextInt(4))%2==0? num:(-1)*num;
        String snum=num>=0? "+"+num: ""+num;
        int iterm=(1+r.nextInt(5));
        iterm = (1+r.nextInt(4))%2==0? iterm:(-1)*iterm;
        String siterm=iterm>=0? "+"+iterm : ""+iterm;
        /*if(alg==0){
            radicand+= (snum)+"x^"+exp+" "+siterm;
            fin = caretToExponent(singleDigitRoot(""+root,radicand));
        }
        else if(alg==1){
        */
            //radicand= (2+r.nextInt(5))+"^"+exp;
            radicand= (snum)+"x^"+exp+" "+siterm;
            fin = caretToExponent(singleDigitRoot(""+2,radicand));
            
            values = rootExpNumAlg(r);
            //values[1]= (exp%2 != 0)? values[1] : ( values[1]%2 != 0? values[1]: ++values[1]);
            values[1]=(1+r.nextInt(4))%2==0? 2 : 1;
            num = values[2];
            num = (1+r.nextInt(4))%2==0? num:(-1)*num;
            snum=num>=0? "+"+num: ""+num;
            
            String term2=(snum)+"x^"+values[1]+" "+siterm;
            term2=caretToExponent(singleDigitRoot(""+2,term2));
            //term2 = (1+r.nextInt(4))%2==0? "+"+term2:"-"+term2;
            
            num = values[2];
            num = (1+r.nextInt(4))%2==0? num:(-1)*num;
            snum=num>=0? "+"+num: ""+num;
            
            String sign = (1+r.nextInt(4))%2==0? "+":"-";
            fin= fin+sign+term2+" = "+snum;
            System.out.println(fin);
        /*}
        else {
            radicand= (1+r.nextInt(5))+"x^"+exp;
            fin = caretToExponent(singleDigitRoot(""+root,radicand));
        }
        */
    }
    
    public void inequacoes(int i){
        
        String type = "i";
        Polinomios ea = new Polinomios();
        int pDegree = ea.nTerms(2); //pDegree();
        LinkedList<String> pCoeff = ea.generateTerms(pDegree+1, type);;
        LinkedList<String> mCoeff =  ea.generateTerms(2,type);
                
        //String[] polm = ea.polyn(mCoeff,2);
        //System.out.print("Init d(x)=");
        //ea.printPol(polm);
        /*System.out.print("polyn is: ");
        for(String s: pCoeff){
            System.out.print(s+" ");
        }
        System.out.println();
        
        System.out.print("monom is: ");
        for(String s: mCoeff){
            System.out.print(s+" ");
        }
        System.out.println();*/

        LinkedList<String> extPol = ea.multiply(pCoeff,pDegree,mCoeff,type);
        
        /*System.out.print("extPol is: ");
        for(String s: extPol){
            System.out.print(s+" ");
        }
        System.out.println();*/

        String[] finalPol = ea.polyn(extPol,pDegree+1+1);
        
        System.out.print((i+1)+". ");
        
        String[] ineqtype = {">",">=","<","<="};
        Random r = new Random();
        int index = r.nextInt(3*ineqtype.length)%(ineqtype.length);
        //System.out.print(ineqtype[index]);
        
        //System.out.print("P(x)=");
        ea.stringifyPol(finalPol);
        String polEq = "P(x) = "+ea.stringifyPol(finalPol)+ineqtype[index]+" 0";
        System.out.println(polEq);

        //double a = solveMonomial(mCoeff,"");
        String a = ea.solveMonomial(mCoeff,"");
        System.out.println("Resolva o polinomio, construa a tabela de sinais, e represente o domino sabendo que "+a+ " e' um zero do polinómio");
    }
    
    public void inequacoesSignTablePlusDomainOnly(int n){
        
        Polinomios ea = new Polinomios();
        String zeros = "";
        int pDegree = 1+ea.nTerms(4); //pDegree();
        
        for(int i=0;i<pDegree;i++){
            Random rn =  new Random();
            boolean composites=false;
            int max = 5;
            int num = JogoDeSinais.generateNumber(rn,composites,max);
            int den = JogoDeSinais.generateNumber(rn,composites,max);
            String s = SistemaDeEquacoesLineares.simplifyFraction(num, den);
            zeros+=" "+s;
        }
        
        System.out.print((n+1)+". ");
        
        String[] ineqtype = {">",">=","<","<="};
        int index = 0;
        if(n<ineqtype.length){index=n;} 
        else{
            Random r = new Random();
            index = r.nextInt(3*ineqtype.length)%(ineqtype.length);
        }
        //System.out.print(ineqtype[index]);
        
        String polEq = "P(x) "+ineqtype[index]+" 0";
        System.out.println(polEq);
        
        System.out.println("Construa a tabela de sinais e represente o domino sabendo que os zeros de P(x), cujo grau e' "+pDegree+", sao: "+zeros);
    }
    
    public void equacoesExponenciais(int i, int expDegree, boolean ineq){
        Random r = new Random();
        int base = 2+r.nextInt(30);
        
        int xcoef = generateCoeff(r);
        String sxcoef="";
        int x2coef = 1;
        if(expDegree==2){x2coef = generateCoeff(r);}
        
        int indcoef = generateCoeff(r);
        String sindcoef = indcoef>=0? "+"+indcoef : ""+indcoef;
        
        if(expDegree==2) sxcoef = xcoef>=0? "+"+xcoef : ""+xcoef;
        
        int eqcoef = generateCoeff(r);
        
        String eqVsIneq= ineq? ( ((1+r.nextInt(8))%2)==0? " >= " : " <= " ) : " = ";
        
        String equation= "";
        if(indcoef >=-4 && indcoef <=4){
            if(indcoef<0) indcoef*=-1;
            int mult=1;
            for(int index =0;index<indcoef; index++){mult*=indcoef;}
            if(expDegree==2) equation = mult+"*"+base+"^("+x2coef+"x^2"+sxcoef+"x) "+ eqVsIneq +base+"^"+eqcoef;
            else equation = mult+"*"+base+"^("+xcoef+"x) "+ eqVsIneq +base+"^"+eqcoef;
        } else{
            if(expDegree==2)equation = ""+base+"^"+"("+x2coef+"x^2"+sxcoef+"x"+sindcoef+") " + eqVsIneq +base+"^"+eqcoef;
            else equation = ""+base+"^"+"("+xcoef+"x"+sindcoef+") "+ eqVsIneq +base+"^"+eqcoef;
        }
        
        System.out.print(i+". ");
        System.out.println(equation);
    }
    
    public void equacoesExponenciaisQuadratica(int i, int expDegree, boolean ineq){
        equacoesExponenciais(i,2,ineq);
    }
    
    public void inequacoesExponenciais(int i, int expDegree, boolean ineq){
        equacoesExponenciais(i,2,ineq);
    }
    
    public void inequacoesExponenciaisQuadratica(int i, int expDegree, boolean ineq){
        equacoesExponenciaisQuadratica(i,2,ineq);
    }
    
    protected int generateCoeff(Random r){
        int sign = r.nextInt(8)%2;
        int num = 2+r.nextInt(8);
        num = sign==1? (-1)*num : num;
        return num;
    }
    
    protected int[] rootExpNumAlg(Random r){
        int root = (2+r.nextInt(6));
        int exp = (1+r.nextInt(root-1));
        int num = (1+r.nextInt(5));
        int alg = (1+r.nextInt(9))%3;
        int [] ret = new int[4];
        ret[0]=root;
        ret[1]=exp;
        ret[2]=num;
        ret[3]=alg;
        return ret;
    }
    
    public String singleDigitRoot(String root, String num) {
        String result="";
        String s = root;
        root = root.replaceAll("2", "√");
        if(s.equals("2")) return root+"("+num+")";
        root = root.replaceAll("3", "\\^1/3");
        root = root.replaceAll("4", "\\^1/4");
        root = root.replaceAll("5", "\\^1/5");
        root = root.replaceAll("6", "\\^1/6");
        root = root.replaceAll("7", "\\^1/7");
        root = root.replaceAll("8", "\\^1/8");
        root = root.replaceAll("9", "\\^1/9");
        
        result="("+num+")"+root;
        return result;
    }
    
    public String exponent(String root, String num) {
        String result="";
        root = root.replaceAll("0", "\\^0");
        root = root.replaceAll("1", "\\^1");
        root = root.replaceAll("2", "\\^2");
        root = root.replaceAll("3", "\\^3");
        root = root.replaceAll("4", "\\^4");
        root = root.replaceAll("5", "\\^5");
        root = root.replaceAll("6", "\\^6");
        root = root.replaceAll("7", "\\^7");
        root = root.replaceAll("8", "\\^8");
        root = root.replaceAll("9", "\\^9");
        result="("+num+")"+root;
        //Character.isDigit('0');
        return result;
    }
    
    public String caretToExponent(String root) {
        String result="";
        result=root;
        //Character.isDigit('0');
        return result;
    }
    
    public void test(){
        //System.out.println(superscript("3")+"\u221A"+1+subscript("1"));
        //System.out.println("FOB7");
        
        //System.out.println(singleDigitRoot("3",""+4));
        //System.out.println(caretToExponent("4^2"));
    }
}
