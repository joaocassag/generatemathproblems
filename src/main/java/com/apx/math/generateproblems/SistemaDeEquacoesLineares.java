
package com.apx.math.generateproblems;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class SistemaDeEquacoesLineares {
    
    static int NUM = 5 ;
    public static void main(String[] args){
        System.out.println("Select the number of the operation:");
        System.out.println("1. Sistema de duas equacoes com duas incognitas");
        System.out.println("2. Sistema de tres equacoes com tres incognitas");
        System.out.println("3. Solucoes de sistema de duas equacoes com duas incognitas");
        System.out.println("4. Entre sistema de x equacoes com x incognitas");
        
        
        Scanner scan = null;
        try{
            String arr[];
            int unkn=-1;
            scan = new Scanner(System.in);
            int option = scan.nextInt();
            
            if(option ==1 || option ==2){
                unkn = option+1;
                System.out.println("Resolva os sistemas de equacoes:");
                for(int i=0;i<NUM;++i){
                    System.out.println((i+1)+".");
                    arr=generateXEquationsWithXUnknonwns(unkn);
                    printXEquationsWithXUnknonwns(unkn,arr);
                    System.out.println();
                }
            } else if(option ==3){
                unkn = 2;
                System.out.println("Resolva os sistemas de equacoes:");
                for(int i=0;i<NUM;++i){
                    System.out.println((i+1)+".");
                    arr=generateXEquationsWithXUnknonwns(unkn);
                    printXEquationsWithXUnknonwns(unkn,arr);
                    solveEquationWithTwoUnknonwns(arr);
                    System.out.println();
                }
            } else if(option ==4){
                System.out.println("Por favor entre um sistema de duas equacoes");
                System.out.println("O programa para de receber inputs quando o necessario ja foi recebido");
                //System.out.println("Please note that when enough valid terms have been received,\n the remaining ones will somply be ignored");
                unkn = 2;
                arr = readXEquationsWithXUnknonwns(unkn);
                //printXEquationsWithXUnknonwns(unkn,arr);
                solveEquationWithTwoUnknonwns(arr);
                System.out.println();
            } else if(option ==5){
                int num = -3;
                int den = 2;
                String s = simplifyFraction(num,den);
                System.out.println(num+"/"+den+" = "+s);
            } else{
                System.out.println("The valid options are between 1 and 4");
                return;
            }
            
        }catch(Exception e){
        
        }finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occurred closing scanner due to "+e.getMessage());}
        }
        
    }
    public static String[] generateXEquationsWithXUnknonwns(int n){
        Random rn = new Random();
        String [] arr = new String[(n+1)*n];
        //int value;
        int sign;
        for(int i =0;i<arr.length;i++){
            sign = (rn.nextInt(6))%2;
            //value = (sign==0)? 1+rn.nextInt(8) : (-1)*1+rn.nextInt(8);
            arr[i]=(sign==0)? ("+"+ (1+rn.nextInt(8)) ) : ("-"+ (1+rn.nextInt(8)) );
        }
        return arr;
    }
    
    public static void printXEquationsWithXUnknonwns(int n, String[] arr){
        int count=0;
        int r = n+1;
        for(int i =0;i<arr.length;i++){
            if( (count%r)==0) System.out.print(arr[i]+"x ");
            else if((count%r)==1) System.out.print(arr[i]+"y ");
            else if( (count%r)==2){
                if(count == n) System.out.print(" = "+arr[i]);
                else System.out.print(arr[i]+"z");
            } else if( (count%r)==3 ){
                if(count == n) System.out.print(" = "+arr[i]);
                else System.out.print(arr[i]+"w");//for the case of 4 unkonwns
            }
            count++;
            if(count==r){
                System.out.println();// add new line
                count=0;
            }
        }
        System.out.println();
    }
    
    public static String[] readXEquationsWithXUnknonwns(int n){
        
        int terms = (n+1)*n;
        //TODO:
        //n=2 equations
        // 10 + 11 = 12
        // 20 + 21 = 22
        
        Scanner scan = new Scanner(System.in);
        
        int count = 0;
        String finalInput="";
        String input="";
        boolean validLine=true;
        while(count<terms){
            System.out.println("Please enter the next line of coefficients:");
            input = scan.nextLine();
            
            if(!input.contains(" ")){
                System.out.println("coefficients have to be separated by spaces ie. 2 1 3");
                continue;
            }
            
            while(input.contains("\t") || input.contains("  ")){
                if(input.contains("\t"))input = input.replace("\t"," ");//replace tab with single space
                if(input.contains("  "))input = input.replace("  "," ");//replace double space with single space
            }
            
            if(input.equals(" ")){
                System.out.println("please enter valid inputs ie. 2 1 3");
                continue;
            }
            String[] numbers = input.split(" ");
            
            //TODO: validate things like + - alone ie. +2 is valid, but + 2 should not be
            // + or - followed by space is not valid
            for(String num: numbers){
                for(int i=0; i<num.length();i++){
                    if(!Character.isDigit(num.charAt(i)) && (num.charAt(i)!='+' && num.charAt(i)!='-')){
                        validLine = false;
                        System.out.println(num.charAt(i)+ " from "+num+ " is not digit");
                        System.out.println(num+ " is not valid");
                        break;
                    }
                }
                if(validLine == false){
                    //TODO: What would you like todo:
                    //1) correct number at position x
                    //2) reenter the whole line
                    //3) reenter all equations
                    break;
                }
            }
            
            if(validLine ==true){
               if(!input.endsWith(" ")) input+=" ";
               finalInput += input; 
               //System.out.println("finalInput is "+finalInput);
               count+=numbers.length;
            } else{ continue;}
            
            if(count>=terms){
                //we have received enough terms
                break;
            }
            
            System.out.println("We have received "+count+" numbers. "+(terms-count)+" more missing");
        }
        scan.close();
        //finalInput = finalInput.substring(0,terms);
        
        String[] arr = new String[(n+1)*n];
        String[] entered = finalInput.split(" ");
        for(int i=0;i<arr.length;i++){
            arr[i]=entered[i].startsWith("-")? (entered[i]): ("+"+(entered[i]) );
        }
        
        System.out.println("The equations received are:");
        printXEquationsWithXUnknonwns(n,arr);
        
        //n= 3 equations
        // 10 + 11 + 12 = 13
        // 20 + 21 + 22 = 23
        // 30 + 31 + 32 = 33
        
        return arr;
    }
    
    public static void solveEquationWithTwoUnknonwns(String[] arr){
        //double x1, x2, y1, y2, r1, r2;
        // x1 + y1 = r1
        // x2 + y2 = r2
        
       //delta = x1*y2-y1x2
       int delta = Integer.parseInt(arr[0]) * Integer.parseInt(arr[4]) - Integer.parseInt(arr[1])*Integer.parseInt(arr[3]);
       //System.out.println("delta is "+delta);
       if(delta == 0){
           System.out.println("Either there are infinitely many solutions, or there are no solutions at all");
           //TODO: 1) Inconsistent; 2) Equivalent equations
           return;
       }
        
      //delta_x = r1*y2 - y1*r2
      int delta_x = Integer.parseInt(arr[2]) * Integer.parseInt(arr[4]) - Integer.parseInt(arr[1])*Integer.parseInt(arr[5]);
      //System.out.println("delta_x is "+delta_x);
      
      //delta_y = x1*r2 - r1*x2
      int delta_y = Integer.parseInt(arr[0]) * Integer.parseInt(arr[5]) - Integer.parseInt(arr[2])*Integer.parseInt(arr[3]);
      //System.out.println("delta_y is "+delta_y);
      
      //x = delta_x/delta
      //double x = delta_x/delta;
      //String x = ""+delta_x+"/"+delta;
      String x= simplifyFraction(delta_x, delta);
      
      //y = delta_y/delta
      //double y = delta_y/delta;
      //String y = ""+delta_y+"/"+delta;
      String y= simplifyFraction(delta_y, delta);
      
      System.out.println("x is "+x);
      System.out.println("y is "+y);
    }
    
    public static String simplifyFraction(int num, int den){
        
        if(den == 0) return "Denominador nao pode ser 0";
        if(num == 0) return "0";
        
        //TODO: Some systems do not print the result. ie:
        /*
        //Does not print
        -8x +5y  = +3
        +7x +7y  = +4

        Prints:
        +1x +2y  = +3
        +4x +5y  = +6

        x is -1
        y is 2
        */
        
        LinkedList<Integer> primes = findPrimeNumbersUpToMax(num,den);
        //System.out.print("primes is: ");
        //printList(primes);
        
        LinkedList<Integer> fPrimesNum= decomposeIntoPrimes(num,primes);
        LinkedList<Integer> fPrimesDen= decomposeIntoPrimes(den,primes);
        
        //System.out.print("fPrimesNum is: ");
        //printList(fPrimesNum);
        //System.out.print("fPrimesDen is: ");
        //printList(fPrimesDen);
        int discard = -1;//NOTE: this is necessary becasue of the overloaded LinkedList.remove() method ie. we want remove(int)
        for(int i=0; i<fPrimesNum.size();i++){
            for(int j=0;j<fPrimesDen.size();j++){
                if(fPrimesDen.get(j).intValue() == fPrimesNum.get(i).intValue()){
                    //System.out.println("fPrimesNum.get(i) "+(fPrimesNum.get(i))+" is the same as fPrimesDen.get(j) "+(fPrimesDen.get(j))+". Removing "+fPrimesDen.get(j));
                    fPrimesNum.add(i, 0);
                    discard = fPrimesNum.remove(i+1);
                    
                    fPrimesDen.add(j,0);
                    discard= fPrimesDen.remove(j+1);
                    //System.out.print("fPrimesNum is now: ");
                    //printList(fPrimesNum);
                    //System.out.print("fPrimesDen is now: ");
                    //printList(fPrimesDen);
                }
            }
        }
        
        int ns = 1;
        for(int i: fPrimesNum){
            if(i!=0)ns*=i;
        }
        
        int ds = 1;
        for(int i: fPrimesDen){
            if(i!=0)ds*=i;
        }
        //String sds= (ds==1)? "" : ""+ds;
        String ret = (ds==1)? ""+ns : ns+"/"+ds;
        ret = (num*den)>0? ret: "-"+ret;
        return ret;
    }
    
    public static void printList(LinkedList<Integer> l){
        for(int i: l){System.out.print(i+" ");}
        System.out.println();
    }
    
    public static LinkedList<Integer> decomposeIntoPrimes(int a, LinkedList<Integer> primes){
        //System.out.println("Entered decomposeIntoPrimes");
        LinkedList<Integer> primeFactors = new LinkedList<>();
        int n = a>=0? a:-a;
        if(n<2){ return primeFactors;}
        if(n==2){
            primeFactors.add(2);
            return primeFactors;
        }
        
        for(int i=0;i<primes.size();i++){
            while(n>1 && (n%primes.get(i)) == 0){
                primeFactors.add(primes.get(i));
                n/=primes.get(i);
            }
            if(n==1) {
                //System.out.print("primes is : ");
                //printList(primeFactors);
                //System.out.println("Leaving decomposeIntoPrimes");
                return primeFactors;
            }
        }
        //System.out.print("primes is : ");
        //printList(primeFactors);
        //System.out.println("Leaving decomposeIntoPrimes");
        return primeFactors;
    }
    
    public static LinkedList<Integer> findPrimeNumbersUpToMax(int a, int b){
        //System.out.println("Entered findPrimeNumbersUpToHalfOfMax");
        a = a>0? a:-a;
        b = b>0? b:-b;
        
        int max = a>b? a:b;
        LinkedList<Integer> primes = new LinkedList<>();
        
        for(int i=2;i<=max;i++){
            if(!isPrime(i)) continue;
            primes.add(i);
        }
        //System.out.print("primes is : ");
        //printList(primes);
                    
        //System.out.println("Leaving findPrimeNumbersUpToHalfOfMax");
        return primes;
    }
    
    public static boolean isPrime(int a){
        int n = a>0? a: -a;
        if(n==1) return false;
        for(int i=2;i<(n/2+1);i++){
            if(n%i==0) return false;
        }
        return true;
    }
    
}
