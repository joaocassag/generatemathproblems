
package com.apx.math.generateproblems;

/**
 *
 * @author joaocassamano
 */

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class ExprAlgebFraccoes {
    
    public static void main(String[] args){
        ExprAlgebFraccoes ea = new ExprAlgebFraccoes();
        String type = "";
        Scanner scan = null;
        try{
            scan = new Scanner(System.in);
            System.out.println("Digite F para gerar termos com fraccoes ou V com variaveis ");
            type = scan.next();
        }catch(Exception e){
            System.out.println("Digite F para gerar termos com fraccoes ou V com variaveis ");
            return;
        }
        
        if(!type.equalsIgnoreCase("v") && !type.equalsIgnoreCase("f")){
            System.out.println("Digite F para gerar termos com fraccoes ou V com variaveis e pressione Enter ");
            return;
        }
        
        for(int i=0;i<5;i++){
            int n = ea.nTerms(type);
            //System.out.println("n: "+n);
            LinkedList<String> terms = ea.generateTerms(n,type);

            //System.out.println("terms:");
            //for(String s: terms){System.out.print(s);}
            System.out.println();

            LinkedList<String> par = ea.parenthesize(terms);
            //System.out.println("parenthesize:");
            for(String s: par){System.out.print(s);}
            System.out.println();
        }
        
    }
    
    public LinkedList<String> parenthesize(LinkedList<String> terms){
        int opened = 0;
        int closed = 0;
        boolean action = false;
        int totalParenth = 0;
        boolean open = false;
        int maxParenth = terms.size()/3;
        int mid = terms.size()/2;
        int btw=0;
        Random rn = new Random();
        int openVsClose=(rn.nextInt(2))%2==0?2:3;
        for(int i=0;i<terms.size();i++){
            if(!terms.get(i).equals(" ")){continue;}
            else{
                action = i<mid || (rn.nextInt(2))%2==0;
                if(!action) continue;
                if(i<mid || ((opened+closed)<maxParenth)){
                    open = (opened <=openVsClose*closed) && (rn.nextInt(2))%2==0;
                    if(open){
                        btw=0;
                        opened++;
                        totalParenth++;
                        addOrCloseParenthesis(terms, i,"(");
                    }else{
                        btw++;
                        if((closed<opened) && btw>3 && ("a".equals(terms.get(i-1)) || "b".equals(terms.get(i-1)) ) ){
                            closed++;
                            totalParenth++;
                            addOrCloseParenthesis(terms, i,")");
                        }
                    }
                }
                //TODO: eliminate terms of the type (a-a)(...) or (...)(b-b)
            }
        }
        if(closed<opened){
            String par = "";
            for(int p=0;p<(opened-closed);p++){par+=")";}
            //System.out.println("final par is "+par);
            addOrCloseParenthesis(terms,terms.size()-1 ,par);
        }
        return terms;
    }
    
    public void addOrCloseParenthesis(LinkedList<String> terms, int i, String par){
        //System.out.println("i "+i+" add: "+par);
        terms.add(i, par);
        terms.remove(i+1);
    }
    
    
    public LinkedList<String> generateTerms(int nTerms, final String type){
        //Random rn = new Random();
        LinkedList<String> l = new LinkedList<>();
        l.add(" ");
        int sign =0;
        int varType =0;
        int coef =1;
        
        for(int i=0;i<nTerms;i++){
            Random rn = new Random();
            sign = (rn.nextInt(2))%2;
            l.add(" ");
            l.add(sign==0?"+":"-");
            
            coef = (rn.nextInt(3))%3;
            
            rn = new Random();
            varType = (rn.nextInt(2))%2;
            l.add(" ");
            
            //Random rn_c = new Random(3);
            //coef = (rn_c.nextInt(3))%3;
            //l.add(varType==0?(coef+"a"):(coef+"b"));
            String[] terms = getTerms(type);
            //String var = varType==0?("a"):("b");
            String var = (varType==0?(terms[0]):(terms[1]));
            String toAdd = var;
            l.add(toAdd);
            l.add(" ");
        }
        return l;
    }
    
    public String[] getTerms(final String type){
        String _type = type;
        if(!type.equalsIgnoreCase("v") && !type.equalsIgnoreCase("f")){
            System.out.println("Warning: Did not provide F or V, setting V by default ...");
            _type="v";
        }
        
        String values[] = {"a","b"};
        if(_type.equalsIgnoreCase("v")){
           //variables
        } else if(_type.equalsIgnoreCase("f")){
            int[] numbers = fractionNumbers();
            values[0]=numbers[0]+"/"+numbers[1];
            values[1]=numbers[2]+"/"+numbers[3];
        }
        return values;
    }
    
    public int[] fractionNumbers(){
        Random rn = new Random();
        //Random rn = null;
        int[] numbers = new int[4];
        int n=2;
        for(int i=0;i<4;i++){
            //rn = new Random(System.nanoTime());
            n= (1+rn.nextInt(4));
            if (n==1){n= (i==1||i==3)? 2 : n;}
            
            numbers[i] = n;
        }
        return numbers;
    }
    
    
    public int nTerms(String type){
        //Random rn = new Random();
        Random rn = null;
        if(type.equalsIgnoreCase("v")){
            rn = new Random(System.nanoTime());
            return (6+rn.nextInt(6));
        }
        rn = new Random(System.nanoTime());
        return (2+rn.nextInt(3));
        //return (6+rn.nextInt(6));
    }
    
}
