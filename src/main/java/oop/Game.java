
package oop;

import java.util.Random;
import java.util.Scanner;

public abstract class Game {
    int TRIES = 3;
    /* These variables will be added in the next iterations of this program
    //int LEVEL = 0;
    //int POINTS = 0;
    */
    
    abstract public void guess();
    abstract public void setTries();
    abstract public void intro();
    abstract public void level();
    
}
