package oop;

import java.util.Random;
import java.util.Scanner;

public class Vowel extends Game {
    @Override
    public void guess(){
        Scanner scan = null;
        try{
            String vowels="aeiou";
            Random rn = new Random();
            int index = rn.nextInt(5);
            System.out.println("The secret vowel has been chosen");
            char value = vowels.charAt(index);
            scan = new Scanner(System.in);
            int guess = -1;

            intro();
            setTries();
            int triesRem=TRIES;

            System.out.println("You have "+TRIES+"!");
            System.out.println("### value is "+value);
            for(int i=0; i<TRIES;i++){
                System.out.println("Please enter your guess:");
                guess = scan.next().charAt(0);
                if(guess == value){
                    System.out.println("Your guess is corect! The secret vowel is "+value+". Well done.");
                    return;
                }else{
                    triesRem--;
                    System.out.println("Your guess is incorrect!");
                    if(triesRem>0)System.out.println("Number of trials left: "+triesRem+" left. Try again:");
                }
            }
            System.out.println("You lost! You might have more luck  next time! ");
        }finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occured closing scanner due to "+e.getMessage());}
        }
    }
    
    @Override
    public void setTries(){
        TRIES = 2;
    }
    
    @Override
    public void intro(){
        System.out.println("Welcome to the VowelGuessing game");
        System.out.println("TODO: TODO: TODO: Instructions");
    }
    
    @Override
    public void level(){}
}
