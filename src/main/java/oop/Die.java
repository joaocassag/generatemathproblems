package oop;

import java.util.Random;
import java.util.Scanner;

public class Die extends Game {
    
    @Override
    public void guess(){
        
        Scanner scan = null;
        try{
            Random rn = new Random();
            int value = 1+rn.nextInt(7);
            System.out.println("The secret side has been chosen");
            scan = new Scanner(System.in);
            int guess = -1;

            intro();
            setTries();
            int triesRem=TRIES;

            System.out.println("You have "+TRIES+" trials!");
            
            //Use this next line in the development/debugging phase
            System.out.println("### value is "+value);
            
            for(int i=0; i<TRIES;i++){
                System.out.println("Please enter your guess:");
                guess = scan.nextInt();
                
                //TODO: check if player has already entered this guess and tell them to enter a different one
                if(guess == value){
                    System.out.println("Your guess is corect. The secret number is "+value+". Well done!");
                    return;
                }else{
                    triesRem--;
                    System.out.println("Your guess is incorrect!");
                    if(triesRem>0)System.out.println("Number of trials left: "+triesRem+" left. Try again:");
                }
            }
            System.out.println("You lost! You might have more luck  next time! ");
        }finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occured closing scanner due to "+e.getMessage());}
        }
    }
    
    @Override
    public void setTries(){
        TRIES = 3;
    }
    
    @Override
    public void intro(){
        System.out.println("Welcome to the DieGuessing game!");
        System.out.println("Instructions: The DieGuessing game consists of trying to guess a secret side of a die.\n"
                + "That is selected by the computer in the beginning of the game.\n"
                + "The player has a certain number of trials to guess the secret side.\n"
                + "The player loses if they fail to guess the secret side in the number of trial.\n"
                + "They will win otherwise.\n");
    }
    
    @Override
    public void level(){
        //TODO:
    }
    
}
