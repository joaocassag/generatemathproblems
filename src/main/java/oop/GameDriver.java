
package oop;

import java.util.Scanner;


public class GameDriver {
    
    public static void main(String[] args){
        System.out.println("Please choose the number of the guessing game you would like to play:");
        System.out.println("1. Die");
        System.out.println("2. Vowel");
        System.out.println("3. Number");
        System.out.println("4. Letter");
        System.out.println("5. UnfairCoin");
        
        Scanner scan = null;
        try{
            scan = new Scanner(System.in);
            int option = scan.nextInt();
            if(option ==1){
                Game game = new Die();
                game.guess();
            } else if(option ==2){
                //vowelGuess();
            } else if(option ==3){
                //numberGuess();
            }else if(option ==4){
                //letterGuess();
            } else{
                System.out.println("The valid options are between 1 and 4");
                return;
            }
        }catch(Exception e){System.out.println("An error occured due to "+e.getMessage());}
        finally{
            try{if(scan!=null)scan.close();}
            catch(Exception e){System.out.println("An error occured closing scanner due to "+e.getMessage());}
        }
    }
    
}
